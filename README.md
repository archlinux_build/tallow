# tallow

Block hosts that attempt to bruteforce SSH using the journald API

Read more here:
https://github.com/clearlinux/tallow

Download tallow package for Archlinux from here:
[tallow](https://gitlab.com/archlinux_build/tallow/-/jobs/artifacts/master/browse?job=run-build)